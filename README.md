This mod is designed to allow more flexibility within the mythic class system by unbinding mythic classes from fixed casting/scaling attributes.

Current release is available: https://www.nexusmods.com/pathfinderwrathoftherighteous/mods/3
